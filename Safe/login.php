<?php
session_start();

include("connection.php");
include("functions.php");

if($_SERVER['REQUEST_METHOD'] == "POST"){

    $username = $_POST['username'];
    $pass = $_POST['password'];

    if(!empty($username) && !empty($pass) && !is_numeric($username)){
        $stmt = $con->prepare("SELECT * FROM testusers WHERE user_name = ? limit 1");
        $stmt->bind_param("s", $username);
        mysqli_stmt_execute($stmt);
        $result = mysqli_stmt_get_result($stmt);

        if ($result){
            if($result && mysqli_num_rows($result) > 0){

                $user_data = mysqli_fetch_assoc($result);
                
                if($user_data['password'] == $pass){
                    $_SESSION['user_id'] = $user_data['user_id'];
                    header("Location: index.php");
                    die;
                }
            }
        }
        echo '<div style="text-align: center;">Wrong username or password!</div>';   
    }else{
        echo '<div style="text-align: center;">Wrong username or password!</div>';
    }

}

?>


<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
</head>
<body>
    
    <style type="text/css">
        #text{

            height: 25px;
            border-radius: 5px;
            padding: 4px;
            border: solid thin #aaa;
            width: 100%;
        }
        #button{

            padding: 10px;
            width: 100px;
            color: white;
            background-color: orange;
            border: none;
        }
        #box{

            background-color: Black;
            margin: auto;
            width: 300px;
            padding: 20px;
        }
    </style>

    <div id="box">

        <form method="post">
            <div style="font-size: 20px; margin: 10px;color: white;">Login</div>
            <input id="text" type="text" name="username"><br><br>
            <input id="text" type="password" name="password"><br><br>

            <input id="button" type="submit" value="Login"><br><br>

            <a href="signup.php" >Sign up</a><br><br>

        </form>


    </div>
</body>
</html>